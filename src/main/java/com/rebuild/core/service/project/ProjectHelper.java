/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.project;

import cn.devezhao.bizz.security.AccessDeniedException;
import cn.devezhao.persist4j.engine.ID;
import com.rebuild.core.Application;
import com.rebuild.core.configuration.ConfigBean;
import com.rebuild.core.configuration.ConfigurationException;
import com.rebuild.core.metadata.EntityHelper;
import com.rebuild.core.privileges.UserHelper;
import com.rebuild.core.service.NoRecordFoundException;

import java.util.Set;


public class ProjectHelper {

    
    public static boolean checkReadable(ID taskOrComment, ID user) {
        try {
            taskOrComment = convert2Task(taskOrComment);

            
            ProjectManager.instance.getProjectByX(taskOrComment, user);
            return true;
        } catch (ConfigurationException | AccessDeniedException ex) {
            return false;
        }
    }

    
    public static boolean isManageable(ID taskOrCommentOrTag, ID user) {
        
        if (UserHelper.isAdmin(user)) return true;

        
        ConfigBean pcfg;
        if (taskOrCommentOrTag.getEntityCode() == EntityHelper.ProjectTaskTag) {
            Object[] projectId = Application.getQueryFactory().uniqueNoFilter(taskOrCommentOrTag, "projectId");
            pcfg = ProjectManager.instance.getProject((ID) projectId[0], null);
        } else {
            pcfg = ProjectManager.instance.getProjectByX(convert2Task(taskOrCommentOrTag), null);
        }
        
        
        if (user.equals(pcfg.getID("principal"))) return true;
        
        if (!pcfg.get("members", Set.class).contains(user)) return false;

        
        Object[] createdBy = Application.getQueryFactory().uniqueNoFilter(taskOrCommentOrTag, "createdBy");
        return createdBy != null && createdBy[0].equals(user);
    }

    
    private static ID convert2Task(ID taskOrComment) {
        if (taskOrComment.getEntityCode() == EntityHelper.ProjectTaskComment) {
            Object[] o = Application.getQueryFactory().uniqueNoFilter(taskOrComment, "taskId");
            if (o == null) {
                throw new NoRecordFoundException(taskOrComment);
            }

            return (ID) o[0];
        }
        return taskOrComment;
    }
}
