/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.core.service.dashboard.charts;


public class ChartsHelper {

    
    public static final String VALUE_ZERO = "0";

    
    public static final String VALUE_NONE = "无";

    
    public static boolean isZero(Object value) {
        if (value == null) {
            return true;
        } else if (value instanceof Double) {
            return (Double) value == 0d;
        } else if (value instanceof Long) {
            return (Long) value == 0L;
        } else if (value instanceof Integer) {
            return (Integer) value == 0;
        }
        return false;
    }
}
