/*!
Copyright (c) REBUILD <https://getrebuild.com/> and/or its owners. All rights reserved.

rebuild is dual-licensed under commercial and open source licenses (GPLv3).
See LICENSE and COMMERCIAL in the project root for license information.
*/

package com.rebuild.web;


public class WebConstants {

    

    
    public static final String ENV = "env";

    
    public static final String COMMERCIAL = "commercial";

    
    public static final String BASE_URL = "baseUrl";

    
    public static final String HOME_URL = "homeUrl";

    
    public static final String APP_NAME = "appName";

    
    public static final String STORAGE_URL = "storageUrl";

    
    public static final String FILE_SHARABLE = "fileSharable";

    
    public static final String MARK_WATERMARK = "markWatermark";

    
    public static final String PAGE_FOOTER = "pageFooter";

    

    
    public static final String LOCALE = "locale";

    
    public static final String CSRF_TOKEN = "csrfToken";

    
    public static final String AUTH_TOKEN = "authToken";

    

    
    public static final String $BUNDLE = "bundle";

    
    public static final String $USER = "user";
}
